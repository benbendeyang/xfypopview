#
# Be sure to run `pod lib lint XFYPopView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XFYPopView'
  s.version          = '0.4.3'
  s.summary          = '弹窗视图'
  s.swift_version    = '4.2'
  
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'git@gitlab.com:benbendeyang/xfypopview'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'leonazhu' => '412038671@qq.com' }
  s.source           = { :git => 'git@gitlab.com:benbendeyang/xfypopview.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'XFYPopView/Classes/**/*'
  s.resource_bundles = { 'XFYPopView' => ['XFYPopView/Assets/*.{xib,xcassets}'] }
  s.frameworks = 'UIKit'
    
#  s.subspec 'PopSelect' do |a|
#    a.source_files = 'XFYPopView/Classes/PopSelect/*'
#    a.resource_bundles = {
#      'PopSelect' => ['XFYPopView/Assets/PopSelect/*.{xib,xcassets}']
#    }
#  end

  # s.resource_bundles = {
  #   'XFYPopView' => ['XFYPopView/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

end
