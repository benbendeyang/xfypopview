//
//  SingleSelectCell.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/4/15.
//

import UIKit

class SingleSelectCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            contentLabel.font = PopSelectManager.shared.selectedFont
            contentLabel.textColor = PopSelectManager.shared.selectedColor
        } else {
            contentLabel.font = PopSelectManager.shared.normalFont
            contentLabel.textColor = PopSelectManager.shared.normalColor
        }
    }
}
