//
//  PopViewManager.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/3/16.
//

import Foundation

open class PopViewManager: NSObject {

    public static let shared = PopViewManager()

    private var backgroundView = UIView(frame: UIScreen.main.bounds)
    private var backgroundButton = UIButton(frame: UIScreen.main.bounds)
    private var popView: PopContentView?
    /// 浏览模式：点击背景关闭视图
    private var isBrowseMode: Bool = true
    /// 背景样式
    private var backgroundStyle: PopViewBackgroundStyle = .black
    /// 位置
    private var position: PopPosition = .center
    /// 动画类型
    private var animateType: PopAnimateType = .scale
    
    private override init() {
        super.init()
        backgroundButton.addTarget(self, action: #selector(clickBackground), for: .touchUpInside)
        backgroundView.addSubview(backgroundButton)
    }
}

// MARK: - 公共方法
public extension PopViewManager {
    
    func show(popView: PopContentView, browseMode: Bool = true, backgroundStyle: PopViewBackgroundStyle = .black, position: PopPosition = .center, animateType: PopAnimateType = .scale) {
        popView.delegate = self
        backgroundView.addSubview(popView)
        self.popView = popView
        self.isBrowseMode = browseMode
        self.backgroundStyle = backgroundStyle
        self.position = position
        self.animateType = animateType
        guard let keywindow = UIApplication.shared.delegate?.window else { return }
        keywindow?.addSubview(backgroundView)
        setupContentViewPosition()
        backgroundAnimation(mode: .show)
        contentViewAnimation(mode: .show)
    }
}

// MARK: - 私有方法
private extension PopViewManager {
    
    @objc private func clickBackground() {
        if isBrowseMode {
            popViewDismiss()
        }
    }
    
    private func setupContentViewPosition() {
        guard let popView = popView else { return }
        switch position {
        case .custom: break
        case .center:
            popView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        case .bottom:
            popView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height - popView.bounds.height/2)
        }
    }
    
    private func contentViewAnimation(mode: PopContentAnimationMode) {
        guard let popView = popView else { return }
        switch animateType {
        case .opacity, .scale:
            let animation = CABasicAnimation(keyPath: animateType.rawValue)
            animation.duration = 0.2
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards
            switch mode {
            case .show:
                animation.fromValue = NSNumber(value: 0)
                animation.toValue = NSNumber(value: 1)
                popView.layer.add(animation, forKey: "contentView-Show")
            case .dismiss:
                animation.delegate = self
                animation.fromValue = NSNumber(value: 1)
                animation.toValue = NSNumber(value: 0)
                popView.layer.add(animation, forKey: "contentView-Dismiss")
            }
        case .dynamic:
            let groupAnimation = CAAnimationGroup()
            groupAnimation.duration = 0.2
            groupAnimation.isRemovedOnCompletion = false
            groupAnimation.fillMode = .forwards
            switch mode {
            case .show:
                let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
                scaleAnimation.fromValue = 0.6
                scaleAnimation.toValue = 1.0
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 0.8
                opacityAnimation.toValue = 1.0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                popView.layer.add(groupAnimation, forKey: "contentView-Show")
            case .dismiss:
                groupAnimation.delegate = self
                let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
                scaleAnimation.fromValue = 1
                scaleAnimation.toValue = 0.6
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 1
                opacityAnimation.toValue = 0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                popView.layer.add(groupAnimation, forKey: "contentView-Dismiss")
            }
        case .rising:
            let groupAnimation = CAAnimationGroup()
            groupAnimation.duration = 0.2
            groupAnimation.isRemovedOnCompletion = false
            groupAnimation.fillMode = .forwards
            let center = popView.center
            switch mode {
            case .show:
                let scaleAnimation = CABasicAnimation(keyPath: animateType.rawValue)
                scaleAnimation.fromValue = NSValue(cgPoint: CGPoint(x: center.x, y: center.y + popView.bounds.height))
                scaleAnimation.toValue = NSValue(cgPoint: center)
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 0.8
                opacityAnimation.toValue = 1.0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                popView.layer.add(groupAnimation, forKey: "contentView-Show")
            case .dismiss:
                groupAnimation.delegate = self
                let scaleAnimation = CABasicAnimation(keyPath: animateType.rawValue)
                scaleAnimation.fromValue = NSValue(cgPoint: center)
                scaleAnimation.toValue = NSValue(cgPoint: CGPoint(x: center.x, y: center.y + popView.bounds.height))
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 1
                opacityAnimation.toValue = 0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                popView.layer.add(groupAnimation, forKey: "contentView-Dismiss")
            }
        }
    }
    
    private func backgroundAnimation(mode: PopContentAnimationMode) {
        switch backgroundStyle {
        case .clear:
            backgroundButton.backgroundColor = .clear
        case .black:
            backgroundButton.backgroundColor = UIColor(white: 0, alpha: 0.3)
            let animation = CABasicAnimation(keyPath: PopAnimateType.opacity.rawValue)
            animation.duration = 0.2
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards
            switch mode {
            case .show:
                animation.fromValue = NSNumber(value: 0)
                animation.toValue = NSNumber(value: 1)
                backgroundButton.layer.add(animation, forKey: "backgroundButton-Show")
            case .dismiss:
                animation.fromValue = NSNumber(value: 1)
                animation.toValue = NSNumber(value: 0)
                backgroundButton.layer.add(animation, forKey: "backgroundButton-Dismiss")
            }
        }
    }
}

// MARK: - 协议
extension PopViewManager: PopViewDelegate {

    public func popViewDismiss() {
        contentViewAnimation(mode: .dismiss)
        backgroundAnimation(mode: .dismiss)
    }
}

extension PopViewManager: CAAnimationDelegate {

    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if popView?.layer.animation(forKey: "contentView-Dismiss") == anim {
            popView?.layer.removeAllAnimations()
            popView?.removeFromSuperview()
            backgroundView.removeFromSuperview()
        }
    }
}
