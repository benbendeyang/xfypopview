//
//  PopSelectManager.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/4/15.
//

import Foundation

open class PopSelectManager {
    
    public static let shared = PopSelectManager()
    public var selectedFont: UIFont = .boldSystemFont(ofSize: 14)
    public var normalFont: UIFont = .systemFont(ofSize: 14)
    public var selectedColor: UIColor = .orange
    public var normalColor: UIColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    
    public let bundle: Bundle = {
        guard let url = Bundle(for: PopSelectManager.self).url(forResource: "XFYPopView", withExtension: "bundle"), let bundle = Bundle(url: url) else {
            return Bundle.main
        }
        return bundle
    }()
    
    public func popSelectView(point: CGPoint, itemSize: CGSize? = nil, titles: [String], selectIndex: Int? = nil, selectBlock: ((_ index: Int) -> Void)? = nil) {
        guard let contentView = PopSelectManager.shared.bundle.loadNibNamed("PopSelectView", owner: nil, options: nil)?.last as? PopSelectView else {
            return
        }
        contentView.layer.anchorPoint = CGPoint(x: 1 - 20/contentView.bounds.width, y: 4/contentView.bounds.height)
        contentView.setView(point: point, itemSize: itemSize, titles: titles, selectIndex: selectIndex, selectBlock: selectBlock)
        PopViewManager.shared.show(popView: contentView, position: .custom, animateType: .scale)
    }
    
    public func popSelectView(at view: UIView, itemSize: CGSize? = nil, titles: [String], selectIndex: Int? = nil, selectBlock: ((_ index: Int) -> Void)? = nil) {
        let point = view.convert(CGPoint(x: view.bounds.width/2, y: view.bounds.height), to: UIApplication.shared.windows.last)
        popSelectView(point: point, itemSize: itemSize, titles: titles, selectIndex: selectIndex, selectBlock: selectBlock)
    }
}
