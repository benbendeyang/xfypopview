//
//  PopViewController.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/3/18.
//
//  ⚠️contentView继承类不可命名为PopView

import UIKit

open class PopViewController: UIViewController {

    public private(set) var contentView: PopContentView
    private let backgroundButton = UIButton(frame: UIScreen.main.bounds)
    /// 浏览模式：点击背景关闭视图
    public var isBrowseMode: Bool = true
    /// 背景样式
    public var backgroundStyle: PopViewBackgroundStyle = .black
    /// 位置
    public var position: PopPosition = .center
    /// 动画类型
    public var animateType: PopAnimateType = .scale
    
    public init(contentView: PopContentView) {
        self.contentView = contentView
        super.init(nibName: nil, bundle: nil)
        self.contentView.delegate = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public static func pop(on presentController: UIViewController, contentView: PopContentView, browseMode: Bool = true, backgroundStyle: PopViewBackgroundStyle = .black, position: PopPosition = .center, animateType: PopAnimateType = .scale) {
        let popViewController = PopViewController(contentView: contentView)
        popViewController.modalPresentationStyle = .custom
        popViewController.isBrowseMode = browseMode
        popViewController.backgroundStyle = backgroundStyle
        popViewController.position = position
        popViewController.animateType = animateType
        presentController.present(popViewController, animated: false)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initController()
    }
    
    override open var prefersStatusBarHidden: Bool {
        return true
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        backgroundAnimation(mode: .show)
        contentViewAnimation(mode: .show)
    }
}

// MARK: - 私有方法
private extension PopViewController {
    
    /// 初始化
    private func initController() {
        view.backgroundColor = UIColor(white: 0, alpha: 0)
        
        
        backgroundButton.backgroundColor = .clear
        backgroundButton.addTarget(self, action: #selector(clickBackground), for: .touchUpInside)
        view.addSubview(backgroundButton)
        
        view.addSubview(contentView)
        setupContentViewPosition()
    }
    
    private func setupContentViewPosition() {
        switch position {
        case .custom: break
        case .center:
            contentView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        case .bottom:
            contentView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height - contentView.bounds.height/2)
        }
    }
    
    private func contentViewAnimation(mode: PopContentAnimationMode) {
        switch animateType {
        case .opacity, .scale:
            let animation = CABasicAnimation(keyPath: animateType.rawValue)
            animation.duration = 0.2
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards
            switch mode {
            case .show:
                animation.fromValue = NSNumber(value: 0)
                animation.toValue = NSNumber(value: 1)
                contentView.layer.add(animation, forKey: "contentView-Show")
            case .dismiss:
                animation.delegate = self
                animation.fromValue = NSNumber(value: 1)
                animation.toValue = NSNumber(value: 0)
                contentView.layer.add(animation, forKey: "contentView-Dismiss")
            }
        case .dynamic:
            let groupAnimation = CAAnimationGroup()
            groupAnimation.duration = 0.2
            groupAnimation.isRemovedOnCompletion = false
            groupAnimation.fillMode = .forwards
            switch mode {
            case .show:
                let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
                scaleAnimation.fromValue = 0.6
                scaleAnimation.toValue = 1.0
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 0.8
                opacityAnimation.toValue = 1.0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                contentView.layer.add(groupAnimation, forKey: "contentView-Show")
            case .dismiss:
                groupAnimation.delegate = self
                let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
                scaleAnimation.fromValue = 1
                scaleAnimation.toValue = 0.6
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 1
                opacityAnimation.toValue = 0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                contentView.layer.add(groupAnimation, forKey: "contentView-Dismiss")
            }
        case .rising:
            let groupAnimation = CAAnimationGroup()
            groupAnimation.duration = 0.2
            groupAnimation.isRemovedOnCompletion = false
            groupAnimation.fillMode = .forwards
            let center = contentView.center
            switch mode {
            case .show:
                let scaleAnimation = CABasicAnimation(keyPath: animateType.rawValue)
                scaleAnimation.fromValue = NSValue(cgPoint: CGPoint(x: center.x, y: center.y + contentView.bounds.height))
                scaleAnimation.toValue = NSValue(cgPoint: center)
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 0.8
                opacityAnimation.toValue = 1.0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                contentView.layer.add(groupAnimation, forKey: "contentView-Show")
            case .dismiss:
                groupAnimation.delegate = self
                let scaleAnimation = CABasicAnimation(keyPath: animateType.rawValue)
                scaleAnimation.fromValue = NSValue(cgPoint: center)
                scaleAnimation.toValue = NSValue(cgPoint: CGPoint(x: center.x, y: center.y + contentView.bounds.height))
                let opacityAnimation = CABasicAnimation(keyPath: "opacity")
                opacityAnimation.fromValue = 1
                opacityAnimation.toValue = 0
                groupAnimation.animations = [scaleAnimation, opacityAnimation]
                contentView.layer.add(groupAnimation, forKey: "contentView-Dismiss")
            }
        }
    }
    
    private func backgroundAnimation(mode: PopContentAnimationMode) {
        switch backgroundStyle {
        case .clear:
            backgroundButton.backgroundColor = .clear
        case .black:
            backgroundButton.backgroundColor = UIColor(white: 0, alpha: 0.3)
            let animation = CABasicAnimation(keyPath: PopAnimateType.opacity.rawValue)
            animation.duration = 0.2
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards
            switch mode {
            case .show:
                animation.fromValue = NSNumber(value: 0)
                animation.toValue = NSNumber(value: 1)
                backgroundButton.layer.add(animation, forKey: "backgroundButton-Show")
            case .dismiss:
                animation.fromValue = NSNumber(value: 1)
                animation.toValue = NSNumber(value: 0)
                backgroundButton.layer.add(animation, forKey: "backgroundButton-Dismiss")
            }
        }
    }
    
    @objc func clickBackground() {
        if isBrowseMode {
            popViewDismiss()
        }
    }
}

// MARK: - 协议
extension PopViewController: PopViewDelegate {
    
    public func popViewDismiss() {
        backgroundAnimation(mode: .dismiss)
        contentViewAnimation(mode: .dismiss)
    }
}

extension PopViewController: CAAnimationDelegate {
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if contentView.layer.animation(forKey: "contentView-Dismiss") == anim {
            contentView.removeFromSuperview()
            contentView.layer.removeAllAnimations()
            dismiss(animated: false)
        }
    }
}
