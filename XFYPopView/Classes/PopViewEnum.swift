//
//  PopViewEnum.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/3/18.
//
//  枚举

public enum PopContentAnimationMode {
    case show
    case dismiss
}

public enum PopViewBackgroundStyle {
    case clear
    case black
}

public enum PopPosition {
    case custom
    case center
    case bottom
}

public enum PopAnimateType: String {
    case opacity = "opacity"
    case scale = "transform.scale"
    case dynamic = "dynamic"
    case rising = "position"
}
