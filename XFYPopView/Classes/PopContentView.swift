//
//  PopContentView.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/3/16.
//
//  弹窗内容视图

@objc public protocol PopViewDelegate {
    func popViewDismiss()
}

open class PopContentView: UIView {
    public weak var delegate: PopViewDelegate?
}
