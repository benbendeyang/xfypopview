//
//  PopSelectView.swift
//  Pods-XFYPopView_Example
//
//  Created by 🐑 on 2019/4/15.
//

import UIKit

open class PopSelectView: XFYPopView.PopContentView {

    @IBOutlet weak var mainTableView: UITableView!
    private var titles: [String] = [] {
        didSet {
            mainTableView.reloadData()
        }
    }
    private var itemSize: CGSize = CGSize(width: 90, height: 44)
    var selectBlock: ((_ index: Int) -> Void)?
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        let footerView = UIView()
        footerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 10)
        footerView.backgroundColor = .clear
        mainTableView.tableFooterView = footerView
        mainTableView.register(UINib(nibName: "SingleSelectCell", bundle: PopSelectManager.shared.bundle), forCellReuseIdentifier: "SingleSelectCell")
    }
    
    open func setView(point: CGPoint, itemSize: CGSize? = nil, titles: [String], selectIndex: Int? = nil, selectBlock: ((_ index: Int) -> Void)? = nil) {
        if let itemSize = itemSize {
            self.itemSize = itemSize
        }
        let width = self.itemSize.width + 8
        frame = CGRect(x: point.x - (width - 22), y: point.y, width: width, height: CGFloat(titles.count) * self.itemSize.height + 12)
        self.titles = titles
        self.selectBlock = selectBlock
        guard let index = selectIndex, index < titles.count else { return }
        mainTableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
    }
}

// MARK: 列表
extension PopSelectView: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleSelectCell", for: indexPath) as! SingleSelectCell
        cell.contentLabel.text = titles[indexPath.row]
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return itemSize.height
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = .zero
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectBlock?(indexPath.row)
        delegate?.popViewDismiss()
    }
}
