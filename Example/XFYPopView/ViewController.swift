//
//  ViewController.swift
//  XFYPopView
//
//  Created by leonazhu on 03/16/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - 视图调用
    @IBAction func selectPopView(_ sender: UIButton) {
        PopSelectManager.shared.popSelectView(at: sender, titles: ["选项1", "选项二", "长长的选项"], selectIndex: 0) { index in
            print("点击:", index)
        }
    }
    
    @IBAction func normal(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView)
    }
    
    @IBAction func clearStyle(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, backgroundStyle: .clear)
    }
    @IBAction func blackStyle(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, backgroundStyle: .black)
    }
    
    @IBAction func browseMode(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, browseMode: true)
    }
    @IBAction func noBrowseMode(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, browseMode: false)
    }
    
    @IBAction func custom(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        contentView.frame = CGRect(x: 60, y: 100, width: 200, height: 200)
        PopViewManager.shared.show(popView: contentView, position: .custom)
    }
    @IBAction func center(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        contentView.frame = CGRect(x: 60, y: 100, width: 200, height: 200)
        PopViewManager.shared.show(popView: contentView, position: .center)
    }
    @IBAction func bottom(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        contentView.frame = CGRect(x: 60, y: 100, width: 200, height: 200)
        PopViewManager.shared.show(popView: contentView, position: .bottom)
    }
    
    @IBAction func opacity(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, animateType: .opacity)
    }
    @IBAction func scale(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, animateType: .scale)
    }
    @IBAction func dynamic(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, animateType: .dynamic)
    }
    @IBAction func rising(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewManager.shared.show(popView: contentView, animateType: .rising)
    }
    
    // MARK: - 控制器调用
    @IBAction func controllerBrowseMode(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewController.pop(on: self, contentView: contentView, browseMode: true)
    }
    @IBAction func controllerNoBrowseMode(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewController.pop(on: self, contentView: contentView, browseMode: false)
    }
    
    @IBAction func controllerClearStyle(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewController.pop(on: self, contentView: contentView, backgroundStyle: .clear)
    }
    @IBAction func controllerBlackStyle(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        PopViewController.pop(on: self, contentView: contentView, backgroundStyle: .black)
    }
    
    @IBAction func controllerCustom(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        contentView.frame = CGRect(x: 60, y: 100, width: 200, height: 200)
        PopViewController.pop(on: self, contentView: contentView, position: .custom)
    }
    @IBAction func controllerCenter(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        contentView.frame = CGRect(x: 60, y: 100, width: 200, height: 200)
        PopViewController.pop(on: self, contentView: contentView, position: .center, animateType: .dynamic)
    }
    @IBAction func controllerBottom(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        contentView.frame = CGRect(x: 60, y: 100, width: 200, height: 200)
        PopViewController.pop(on: self, contentView: contentView, position: .bottom, animateType: .rising)
    }
    
    @IBAction func customPopViewController(_ sender: UIButton) {
        guard let contentView = Bundle.main.loadNibNamed("ContentView", owner: nil, options: nil)?.last as? ContentView else {
            return
        }
        contentView.titleLabel.text = sender.currentTitle
        CustomPopViewController.pop(on: self, contentView: contentView)
    }
}
