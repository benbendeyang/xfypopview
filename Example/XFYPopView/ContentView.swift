//
//  ContentView.swift
//  XFYPopView_Example
//
//  Created by 🐑 on 2019/3/18.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

class ContentView: PopContentView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
    
    @IBAction func close(_ sender: Any) {
        delegate?.popViewDismiss()
    }
}
