//
//  CustomPopViewController.swift
//  XFYPopView_Example
//
//  Created by 🐑 on 2019/3/18.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class CustomPopViewController: PopViewController {

    static func pop(on presentController: UIViewController, contentView: PopContentView) {
        let popViewController = CustomPopViewController(contentView: contentView)
        popViewController.modalPresentationStyle = .custom
        popViewController.isBrowseMode = false
        presentController.present(popViewController, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    deinit {
        print("CustomPopViewController销毁")
    }
}

extension CustomPopViewController {
    func initView() {
        print("自定义逻辑")
        contentView.backgroundColor = .orange
    }
}
