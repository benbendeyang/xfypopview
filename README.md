# XFYPopView

[![CI Status](https://img.shields.io/travis/leonazhu/XFYPopView.svg?style=flat)](https://travis-ci.org/leonazhu/XFYPopView)
[![Version](https://img.shields.io/cocoapods/v/XFYPopView.svg?style=flat)](https://cocoapods.org/pods/XFYPopView)
[![License](https://img.shields.io/cocoapods/l/XFYPopView.svg?style=flat)](https://cocoapods.org/pods/XFYPopView)
[![Platform](https://img.shields.io/cocoapods/p/XFYPopView.svg?style=flat)](https://cocoapods.org/pods/XFYPopView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYPopView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYPopView'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYPopView is available under the MIT license. See the LICENSE file for more info.
